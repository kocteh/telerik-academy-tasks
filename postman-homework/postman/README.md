### How to RUN postman test suite

1. First you will need to install:
 - newman
 - newman-reporter-htmlextra

2. run run_test_suite.bat file


### How to install newman & newman-reporter-htmlextra

For newman if you want to install globally use the following command
>$ npm install -g newman

If you want to install locally you need to remove the -g option as follow:
>$ npm install newman


For the newman-reporter-htmlextra you need to run the following command if:
- newman is install globally
>npm install -g newman-reporter-htmlextra
- newman is install locally
>npm install -S newman-reporter-htmlextra

Output of the test suite should be in the following directory
>newman


### Files needed in order to everything go smoothly

1. collection
>SwaggerPetstoreHomework.postman_collection.json

2. environment
>SwaggerPetstoreSetup.postman_environment.json

3. globals
>SwaggerPetstoreGlobal.postman_globals.json


The command in the .bat file should be as follow:

>newman run SwaggerPetstoreHomework.postman_collection.json -g  SwaggerPetstoreGlobal.postman_globals.json -e SwaggerPetstoreSetup.postman_environment.json -r htmlextra

This will run the test suite and will generate HTML report.


## If any question, please don't hesitate to contact me at [kocteh@gmail.com](mailto:kocteh@gmail.com)