## Bellow you will find Test Cases created based on different Testing Techniques

1. **Equivalence partitioning and Boundary Value analysis for the topic title and description;**
2. **Decision tables for the notifications;**
3. **Classification trees for the categories and labels;**
4. **Pairwise testing for the supported browsers, operational systems;**
5. **State transition testing for posting a comment;**

#### 1. Equivalence partitioning and Boundary Value analysis for the topic title and description

Using the **Equivalence partitioning** and **Boundary Value** analysis we will create the following Test Cases:

1. **Equivalence partitioning**:
	* **Тitle**
		* Check for **valid** title length with lenght equal to 78 characters (between 5 and 255);
		* Check for **invalid** title lenght with lenght equal to 300 characters (above upper limit);
		* Check for **invalid** title lenght with lenght euqal to 2 characters (below lower limit);
	* **Description (Post)**
		* Check for **valid** desciption length with lenght equal to 565 characters (between 10 and 32000);
		* Check for **invalid** desciption lenght with lenght equal to 280424 characters (above upper limit);
		* Check for **invalid** desciption lenght with lenght euqal to 7 characters (below lower limit);

| **Test Case ID** | **Pre requisite** | **Test Scenario** | **Test Steps** | **Expected Results** |
|------------------|-------------------|-------------------|----------------|----------------------|
| TC1 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a valid title lenght | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 78 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC2 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for invalid title lenght (above upper limit) | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 300 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Error Message saying the following "Title can't be more than 255 characters" |
| TC3 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for invalid title lenght (below lower limit) | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 2 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Error Message saying the following "Title must be at least 5 characters" |
| TC4 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a valid desciption lenght | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 565 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Desciption of the Topic |
| TC5 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for invalid desciption lenght (above upper limit) | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 280424 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Error Message saying the following "Post must be at least 10 characters" |
| TC6 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for invalid desciption lenght (below lower limit) | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 7 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Pop-up with error message should appear with the following "Body is limited to 32000 characters; you entered 280424." |

2. **Boundary Value analysys**:
	* **Тitle**
		* Check for **exact** boundary value for title length for mininum character (5);
		* Check for **exact** boundary value for title length for maximum character (255);
		* Check for **below** boundary value for title lenght for minimum character (4);
		* Check for **below** boundary value for title lenght for maximum character (254);
		* Check for **above** boundaru value for title lenght for minimum character (6);
		* Check for **above** boundary value for title lenght for maximum character (256);
	* **Description (Post)**
		* Check for **exact** boundary value for desciption length for mininum character (10);
		* Check for **exact** boundary value for desciption length for maximum character (32000);
		* Check for **below** boundary value for desciption lenght for minimum character (9);
		* Check for **below** boundary value for desciption lenght for maximum character (31999);
		* Check for **above** boundaru value for desciption lenght for minimum character (11);
		* Check for **above** boundary value for desciption lenght for maximum character (32001);

| **Test Case ID** | **Pre requisite** | **Test Scenario** | **Test Steps** | **Expected Results** |
|------------------|-------------------|-------------------|----------------|----------------------|
| TC1 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a exact boundary value for title lenght for mininum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 5 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC2 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a exact boundary value for title lenght for maximum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 255 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC3 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a below boundary value for title lenght for mininum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 4 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Error Message saying the following "Title must be at least 5 characters" |
| TC4 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a below boundary value for title lenght for maximum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 254 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC5 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a above boundary value for title lenght for minimum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 6 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC6 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a above boundary value for title lenght for maximum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type title, or paste a link here" enter data with lenght equal to 6 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Error Message saying the following "Title can't be more than 255 characters" |
| TC7 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a exact boundary value for desciption lenght for mininum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 10 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC8 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a exact boundary value for desciption lenght for maximum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 32000 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC9 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a below boundary value for desciption lenght for mininum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 9 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Error Message saying the following "Post must be at least 10 characters" |
| TC10 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a below boundary value for desciption lenght for maximum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 31999 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC11 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a above boundary value for desciption lenght for minimum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 11 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | No error message should pop out regarding the Title of the Topic |
| TC12 | Logged in with a valid account in the forum<br>https://schoolforum.telerikacademy.com/ | Check for a above boundary value for desciption lenght for maximum character | 1. Click on "+ New Topic" button align on the right site of the page<br>3. In the box "Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images" enter data with lenght equal to 32001 characters<br>4. Select "+ Create Topic" button align on the bottom left side of the pop-up | Pop-up with error message should appear with the following "Body is limited to 32000 characters; you entered 280424." |

#### 2. Decision tables for the notifications

In order to create Decision table we need to analyze the requirement(conditions and actions) and create one column with them. In our case we have:

* **Conditions**
	* notifications are enabled;
	* someone liked your post;
	* someone send you a private message;
	* someone replied to your topic;
	* someone replied to your comment;
* **Actions**
	* notification is sent;

The second step is to calculate how many columns are needed in the table. The number of the columns depend on the number of the conditions and the numer of the alternatives for each condition.

Formula = 2^conditions 

2^5 = 32

We will be using **T**(_TRUE_) & **F**(_FALSE_) to fill the table below.

| **Conditions** | R1 | R2 | R3 | R4 | R5 | R6 | R7 | R8 | R9 | R10 | R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18 | R19 | R20 | R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28 | R29 | R30 | R31 | R32 |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| notifications are enabled | T | T | T | T | T | T | T | T | T | T | T | T | T | T | T | T | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F |
| someone liked your post	| T | T | T | T | T | T | T | T | F | F | F | F | F | F | F | F | T | T | T | T | T | T | T | T | F | F | F | F | F | F | F | F |
| someone send you a private message	| T | T | T | T | F | F | F | F | T | T | T | T | F | F | F | F | T | T | T | T | F | F | F | F | T | T | T | T | F | F | F | F |
| someone replied to your topic	| T | T | F | F | T | T | F | F | T | T | F | F | T | T | F | F | T | T | F | F | T | T | F | F | T | T | F | F | T | T | F | F |
| someone replied to your comment	| T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F |
| **Actions** | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |
| notification is sent	| T | T | T | T | T | T | T | T | T | T | T | T | T | T | T | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F |


From here our third step will be to reduce the table. First we can mark ALL the insignificant value with "-". In our case these will apply for ALL columns where the notification is not enabled(**F**).


| **Conditions** | R1 | R2 | R3 | R4 | R5 | R6 | R7 | R8 | R9 | R10 | R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18 | R19 | R20 | R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28 | R29 | R30 | R31 | R32 |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| notifications are enabled | T | T | T | T | T | T | T | T | T | T | T | T | T | T | T | T | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F |
| someone liked your post	| T | T | T | T | T | T | T | T | F | F | F | F | F | F | F | F | -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -
| someone send you a private message	| T | T | T | T | F | F | F | F | T | T | T | T | F | F | F | F | -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -
| someone replied to your topic	| T | T | F | F | T | T | F | F | T | T | F | F | T | T | F | F | -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -
| someone replied to your comment	| T | F | T | F | T | F | T | F | T | F | T | F | T | F | T | F | -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -	| -
| **Actions** | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |
| notification is sent	| T | T | T | T | T | T | T | T | T | T | T | T | T | T | T | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F | F |

Now we can remove from the table the columns that have become indentical. The final table should look like one below:

| **Conditions** | R1 | R2 | R3 | R4 | R5 | R6 |
|-----|-----|-----|-----|-----|-----|-----|
| notifications are enabled 	| T	| T	| T	| T	| T	| F |
| someone liked your post	| T	| F	| F	| F	| F	| - |
| someone send you a private message	| F	| T	| F	| F	| F	| - |
| someone replied to your topic	| F	| F	| T	| F	| F	| - |
| someone replied to your comment	| F	| F	| F	| T	| F	| - |
| **Actions**	| 	| 	| 	| 	| 	|  |
| notification is sent	| T	| T	| T	| T	| F	| F |

Based on the final table we can create at least one Test Case per column and this should give full coverage of the rules for the forum notification.


| **Test Case ID** | **Pre requisite** | **Test Scenario** | **Test Steps** | **Expected Results** |
|------------------|-------------------|-------------------|----------------|----------------------|
| TC1 | Use two different account and device to log in the forum (Account 1 (**AC1**) for Device 1 (**D1**) and Account 2 (**AC2**) for Device 2 (**D2**))<br>https://schoolforum.telerikacademy.com/ | Check if **AC2** will recive notification when they are turned ON and someone liked his post | 1. Turn Notification **ON** for **AC2** from **D2**<br>2. Create new post with **AC2** from **D2**<br>3. Go to the other device **D1** with **AC1** and search for the post created by **AC2**<br>4. Like the post created by **AC2** with **AC1** from **D1**| **AC2** should recive notification that someone liked his post |
| TC2 | Use two different account and device to log in the forum (Account 1 (**AC1**) for Device 1 (**D1**) and Account 2 (**AC2**) for Device 2 (**D2**))<br>https://schoolforum.telerikacademy.com/ | Check if **AC2** will recive notification when they are turned ON and someone sent him a private message | 1. Turn Notification **ON** for **AC2** from **D2**<br>2. Send a private message to **AC2** with **AC1** from **D1**<br>| **AC2** should recive notification for receiving a private message |
| TC3 | Use two different account and device to log in the forum (Account 1 (**AC1**) for Device 1 (**D1**) and Account 2 (**AC2**) for Device 2 (**D2**))<br>https://schoolforum.telerikacademy.com/ | Check if **AC2** will recive notification when they are turned ON and someone replayed to his topic | 1. Turn Notification **ON** for **AC2** from **D2**<br>2. Create new topic with **AC2** from **D2**<br>3. Go to the other device **D1** with **AC1** and search for the topic created by **AC2**<br>4. Replay something to the topic created by **AC2** with **AC1** from **D1**| **AC2** should recive notification that someone replied to his topic |
| TC4 | Use two different account and device to log in the forum (Account 1 (**AC1**) for Device 1 (**D1**) and Account 2 (**AC2**) for Device 2 (**D2**))<br>https://schoolforum.telerikacademy.com/ | Check if **AC2** will recive notification when they are turned ON and someone replied to his comment | 1. Turn Notification **ON** for **AC2** from **D2**<br>2. Create new comment with **AC2** from **D2**<br>3. Go to the other device **D1** with **AC1** and search for the comment created by **AC2**<br>4. Replay something to the comment created by **AC2** with **AC1** from **D1**| **AC2** should recive notification that someone replied to his comment |
| TC5 | Use two different account and device to log in the forum (Account 1 (**AC1**) for Device 1 (**D1**) and Account 2 (**AC2**) for Device 2 (**D2**))<br>https://schoolforum.telerikacademy.com/ | Check if **AC2** will recive notification when they are turned ON but you haven't liked/replayed topic or comment or send him a private message | 1. Turn Notification **ON** for **AC2** from **D2**<br>2. Create new topic with **AC1** from **D1**<br> without mentioning **AC2** | **AC2** should NOT recive any notification |
| TC6 | Use two different account and device to log in the forum (Account 1 (**AC1**) for Device 1 (**D1**) and Account 2 (**AC2**) for Device 2 (**D2**))<br>https://schoolforum.telerikacademy.com/ | Check if **AC2** will recive notification when they are turned OFF | 1. Turn Notification **OFF** for **AC2** from **D2**<br>2. Create new topic with **AC2** from **D2**<br>3. Go to the other device **D1** with **AC1** and search for the topic created by **AC2**<br>4. Like the topic created by **AC2** with **AC1** from **D1**| **AC2** should NOT recive notification that someone liked his topic |

#### 3. Classification trees for the categories and labels

<img src="https://content.screencast.com/users/kocteh/folders/Default/media/08c0c70a-8828-4c3a-8737-773a403a4ee4/Decision_Tree_Simple_Final.png" width="900" height="376">

#### 4. Pairwise testing for the supported browsers, operational systems

Variables that are involved:

 * Supported browsers
 	* Google Chrome Version 76.0.3809.132 (Official Build) (64-bit)
 	* Firefox Quantum Version 68.0.2 (64-bit)
 	* Opera Version:63.0.3368.53
 	* Microsoft Edge 42.17134.1.0

 * Operational Systems
 	* Windows 10 Pro 64-bit Operating System, x64-based processor
 	* Android 9.0 (Pie); MIUI 10
 	* Mac OS

 * Functionality being tested
	* Creating a comment
	* Creating a topic 

Based on the variables that we have (total of 9) if we need to test all possible combination that will be total of 24 Test Cases. 
Using the Pairwise Testing technique we can reduce the number of combination to total of 12. As one of the combination is Mac OS + Microsoft Edge, we can remove that from that table and not making a Test Case for it.

Final table that will be used for the creation of the Test Cases:


| **Supported browsers** | **Operational Systems** | **Functionality being tested** |
|--------------------|---------------------|----------------------------|
| Opera | Windows 10 Pro | Creating a comment |
| Firefox Quantum | Mac OS | Creating a comment |
| Opera | Android 9.0 | Creating a topic |
| Opera | Mac OS | Creating a topic |
| Microsoft Edge | Android 9.0 | Creating a comment |
| ~~Microsoft Edge~~ | ~~Mac OS~~ | ~~Creating a topic~~ |
| Google Chrome | Android 9.0 | Creating a topic |
| Microsoft Edge | Windows 10 Pro | Creating a topic |
| Firefox Quantum | Windows 10 Pro | Creating a topic |
| Google Chrome | Mac OS | Creating a comment |
| Firefox Quantum | Android 9.0 | Creating a topic |
| Google Chrome | Windows 10 Pro | Creating a comment |


###### **TEST CASES** based on the Pariwise Table above:
 * TC1: Test the creation of a **comment** from **Windows 10 PRO OS** by using the **Opera** browser;
 * TC2: Test the creation of a **comment** from **Mac OS** by using the **Firefox Quantum** browser;
 * TC3: Test the creation of a **topic** from **Android 9.0** by using the **Opera** browser;
 * TC4: Test the creation of a **topic** from **Mac OS** by using the **Opera** browser;
 * TC5: Test the creation of a **comment** from **Android 9.0** by using the **Microsoft Edge** browser;
 * TC6: Test the creation of a **topic** from **Android 9.0** by using the **Google Chrome** browser;
 * TC7: Test the creation of a **topic** from **Windows 10 Pro** by using the **Microsoft Edge** browser;
 * TC8: Test the creation of a **topic** from **Windows 10 Pro** by using the **Firefox Quantum** browser;
 * TC9: Test the creation of a **comment** from **Mac OS** by using the **Google Chrome** browser;
 * TC10: Test the creation of a **topic** from **Android 9.0** by using the **Firefox Quantum** browser;
 * TC11: Test the creation of a **comment** from **Windows 10 Pro** by using the **Google Chrome** browser;

#### 5. State transition testing for posting a comment

For this function we have **two** states only:
 * Comment is posted (**valid** input is used);
 * User is blocked from posting the comment (**invalid** input is used)

Here is the State Transition Diagram:

<img src="https://content.screencast.com/users/kocteh/folders/Default/media/1936fb87-289c-45b9-bc5a-614b4bec9f4b/StateTransitioningComment.png" width="900" height="376">