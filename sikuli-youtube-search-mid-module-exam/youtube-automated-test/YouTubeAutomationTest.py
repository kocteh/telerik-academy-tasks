# IMAGES
chrome_url_icon = Pattern("chrome_url_icon.png").similar(0.60)
youtube_search_bar = "youtube_search_bar.png"
youtube_search_button = "youtube_search_button.png"
search_result = "search_result.png"
search_result_thumbnail = "search_result_thumbnail.png"
full_screan = "full_screan.png"


# VARIABLES
chrome_browser_exe = "chrome.exe"
youtube_url = "youtube.com"
youtube_video_searched = "eminem sing for the moment dirty"

# CODE GOES HERE

# Type Window key
type(Key.WIN)
wait(2)

# copy the "chrome.exe" and click enter so the browser can be open
paste(chrome_browser_exe)
wait(1)
type(Key.ENTER)
wait(chrome_url_icon)

# once the browser has loaded, paste in the youtube url in the search bar
paste(youtube_url)
type(Key.ENTER)

# Wait for youtube to load
# click on the search tab in youtube and paste in the the value
# that you would like to search for
wait(youtube_search_bar, 10)
click(youtube_search_bar)
paste(youtube_video_searched)
click(youtube_search_button)

# wait and see if one of the following images are loaded on the screen
# and click on the first one found
wait(0.1)
if exists(search_result):
    click(search_result)
else:
    click(search_result_thumbnail)

#wait for 10 second and then press Space to stop the video
wait(10)
type(Key.SPACE)

# open the video in full screen then wait for 2 second
# and press ESC so you can exit full screen
click(full_screan)
wait(2)
type(Key.ESC)

# close bowser
type(Key.F4, KeyModifier.ALT)

