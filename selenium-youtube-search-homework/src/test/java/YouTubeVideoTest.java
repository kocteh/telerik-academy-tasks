
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import java.util.concurrent.TimeUnit;


public class YouTubeVideoTest {

    private WebDriver driver;
    private  WebDriverWait wait;
    private String baseURL = "https://google.com/";
    private String google_expected_title = "Google";
    private String google_actual_title = "";
    private String youtubeURL = "https://www.youtube.com/";
    private String youtube_expected_title = "YouTube";
    private String youtube_actual_title = "";
    private String youtube_video = "KILLSHOT [Official Audio]";
    private String youtube_video_expected_title = "KILLSHOT [Official Audio] - YouTube";
    private String youtue_video_actual_title = "";

    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 15);
    }

    @AfterClass
    public void tearDown() {
        driver.close();
        driver.quit();
    }


    @Test (priority = 0)
    public void youtubeVideoSearch() {

        driver.get(baseURL);
        google_actual_title  = driver.getTitle();
        Assert.assertEquals(google_expected_title, google_actual_title);

        WebElement searchInput = driver.findElement(By.name("q"));
        wait.until(ExpectedConditions.visibilityOf(searchInput));
        searchInput.sendKeys(youtubeURL);
        searchInput.sendKeys(Keys.ENTER);

        WebElement searchResultYouTube = driver.findElement(By.xpath("//a[@href='"+youtubeURL+"']"));
        wait.until(ExpectedConditions.visibilityOf(searchResultYouTube));
        searchResultYouTube.click();
        youtube_actual_title = driver.getTitle();
        Assert.assertEquals(youtube_expected_title, youtube_actual_title);

        WebElement youtubeSearch = driver.findElement(By.id("search"));
        wait.until(ExpectedConditions.visibilityOf(youtubeSearch));
        youtubeSearch.sendKeys(youtube_video + Keys.ENTER);

        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;

        WebElement eminemKillshot = driver.findElement(By.xpath("//a[@href='/watch?v=FxQTY-W6GIo']"));
        wait.until(ExpectedConditions.visibilityOf(eminemKillshot));
        eminemKillshot.click();
        youtue_video_actual_title = driver.getTitle();
        Assert.assertEquals(youtube_video_expected_title, youtue_video_actual_title);

        WebElement buttonFullScreen = driver.findElement(By.xpath("//button[@title='Full screen (f)']"));
        wait.until(ExpectedConditions.visibilityOf(buttonFullScreen));
        buttonFullScreen.click();

        WebElement buttonPauseVideo = driver.findElement(By.xpath("//button[@title='Pause (k)']"));
        wait.until(ExpectedConditions.visibilityOf(buttonPauseVideo));
        buttonPauseVideo.click();

        WebElement buttonExitFullScreen = driver.findElement(By.xpath("//button[@title='Exit full screen (f)']"));
        buttonExitFullScreen.click();

    }

}
