# Master Test Plan
>### Telerik Academy Forum
>Prepared by: *Ivan Simeonov* <br>
>Date: 27.08.2019

## Table of contents

1. **[INTRODUCTION](#header_introduction)**
2. **[SCOPE/TEST ITEMS](#header_scope)**
3. **[FEATURES TO BE TESTED](#header_features_to_be_tested)**
4. **[FEATURES NOT TO BE TESTED](#header_out_of_scope)**
5. **[APPROACH](#header_approach)**
6. **[ITEM PASS/FAIL CRITERIA](#header_pass_fail_criteria)**
7. **[SUSPENSION CRITERIA AND RESUMPTION REQUIREMENTS](#header_suspension_resumption_criteria)**
8. **[TEST DELIVERABLES](#header_deliverables)**
9. **[ENVIRONMENTAL NEEDS](#header_environment)**
10. **[RESPONSIBILITIES](#header_responsibilities)**
11. **[SCHEDULE](#header_schedule)**
12. **[APPROVALS](#header_approvals)**




### 1. <a name="header_introduction"></a>**INTRODUCTION**
This is a forum that will be used by students and teachers to discuss different topic related to the curriculum. 
The main functions of the forum are:
- **Creating** a topic;
- **Posting** a comment;

### 2. <a name="header_scope"></a>**TEST ITEMS**
We will be testing the two MAIN functionalities of the forum listed below:
- **Creating** a topic;
- **Posting** a comment;


### 3. <a name="header_features_to_be_tested"></a>**FEATURES TO BE TESTED**

**Creation of topic**:

- **Creating** a topic;
- **Creating** a topic **_without_** subject/title;
- **Creating** a **_blank_** topic (no body text);
- **Find** topic under the correct section created;
- **Lock** topic;
- **Delete** topic;

**Posting a comments**:

- **Posting** a comment;
- **Posting** **_blank_** comment (no body text);
- **Posting** a comment with **MIN/MAX** symbols;
- **Formating** finctionalities;
- **Hover** functionalities;
- **Review** comment;
- **Edit** comment;
- **Delete** comment;


### 4. <a name="header_out_of_scope"></a>**FEATURES NOT TO BE TESTED**

Bellow you will find tasks and functionalities that are out of scope and won't be tested during this proccess. All the functionalities and features listed bellow will be integrated and tested by external company.

- **LOG IN/OUT** Functionalities;
- **SEARCH** Functionalty;
- **LIKE** Topic/Comment Functionality;
- **FILTERS** Functionality;


### 5. <a name="header_approach"></a>**APPROACH**

- **Testing Type**: *System testing*;
- **Testing Method**: *Manual (White Box)*;

### 6. <a name="header_pass_fail_criteria"></a>**ITEM PASS/FAIL CRITERIA**

The criteria for these functionality should be 100% Pass for ALL Test Cases.

### 7. <a name="header_suspension_resumption_criteria"></a>**SUSPENSION CRITERIA AND RESUMPTION REQUIREMENTS**
	
**Suspension**:
- If testing on mobile device and application is not mobile compatible;

**Resumption**:
- The page is updated and it's mobile compatible;


### 8. <a name="header_deliverables"></a>**TEST DELIVERABLES**

- **TEST PLAN**
- **TEST CASES**
- **REPORT** (Report file should be in the following format)
	1. File format: .xls
	2. Naming convention: PROJECT_NAME + QA_REPORT + DATE + VERSION
	
	>**TelerikAcademyForum_QA_REPORT_270819_v1.xls**




### 9. <a name="header_environment"></a>**ENVIRONMENTAL NEEDS**

- **Desktop** applications (*Web Broswers*):
    - [Google Chrome](https://www.google.com/chrome/) (Version 76.0.3809.100 (Official Build) (64-bit))
    - [Mozilla Firefox](https://www.mozilla.org/en-US/) (60.8.0 Firefox ESR July 9, 2019)
    - Internet Explorer 11 (Version 11.248.1699.0)

- **Mobile** applications (*Web Broswers*):
    - [Google Chrome](https://www.google.com/chrome/) (Version Version 76.0.3809.111)
        - Android
        - Mac


### 10. <a name="header_responsibilities"></a>**RESPONSIBILITIES**
-	**Ivan Simeonov** (_Junior QA Specialist_): create, execute, report Test Cases;
-	**Pavlina Koleva** (_Teacher/Tutor_): Providing the test environment in order to execute ALL test cases;


### 11. <a name="header_schedule"></a>**SCHEDULE**

| **Task Name** | **Start Date** | **End Date** |
|-----------|------------|----------|
| Making Test Specification | 28/08/19 | 28/08/19 |
| Preform Test Execution | 29/08/19 | 29/08/19 |
| Test Report | 30/08/19 | 30/08/19 |
| Test Delivery | 30/08/19 | 30/08/19 |


### 12. <a name="header_approvals"></a>**APPROVALS**

| **Name (In Capital Letters)** | **Signature** | **Date** |
|---------------------------|-----------|------|
| 1. Ivan Simeonov | | 28/08/19 |
| 2. Vesela Slavkova | | 28/08/19 |
| 3. Boris Stefanov | | 28/08/19 |
| 4. Simeon Georev | | 28/08/19 |
| 5. Styoan Ivanov | | 28/08/19 |
| 6. Pavlina Koleva | | 28/08/19 |
